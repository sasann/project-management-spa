Project management single page application powered by React and Material-ui.

Each task can be either in three states: "Todo", "In Progress" and "Done". Application categorizes tasks based on the state they are at. Board can be used to create a new task which puts them in the todo category. Tasks states can be updated by dragging them into subsequent states columns.

`src/tasks.json` holds initial seed for tasks.

#### runing localy
- Install Node.js
- `npm install`
- `npm run`