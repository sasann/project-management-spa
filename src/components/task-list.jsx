import React, { Component } from 'react';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Box from "@material-ui/core/Box";

import Task from "./task.jsx"

class TaskList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      droppable: false,
    };
    this.taskOver = this.taskOver.bind(this);
    this.taskExit = this.taskExit.bind(this);
    this.taskDrop = this.taskDrop.bind(this);
  }
  taskOver(event) {
    const isTask = event.dataTransfer.types.includes("application/task");
    if ("Todo" !== this.props.title && isTask) {
      console.log("Dropzone");
      event.dataTransfer.dropEffect = "move";
      this.setState({ droppable: true });
      event.preventDefault();
    }
  }

  taskExit(event) {
    this.setState({ droppable: false });
  }

  taskDrop(event) {
    event.preventDefault();
    const taskId = Number(event.dataTransfer.getData("application/task"));
    this.props.taskUpdate({
      id: taskId,
      state: this.props.taskState,
    });
  }
  render() {
    return (
      <Paper elevation={2}>
        <Box
          border={+this.state.droppable}
          p={4}
          onDragOver={this.taskOver}
          onDragLeave={this.taskExit}
          onDrop={this.taskDrop}
        >
          <Box textAlign="center">
            <Typography variant="h5" component="h3">
              {this.props.title}
            </Typography>
          </Box>
          {this.props.items.map((item) => (
            <Task
              key={item.id}
              id={item.id}
              name={item.taskName}
              desc={item.taskDescription}
            />
          ))}
        </Box>
      </Paper>
    );
  }
}

//export default TaskList;
export default TaskList;