import React, { Component } from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';

import _ from 'lodash';
import TaskList from './task-list';
import CreateTask from './create-task'

class Board extends Component {
  constructor(props) {
    super(props);
    this.addTask = this.addTask.bind(this);
    this.updateTask = this.updateTask.bind(this);
    this.state = {
      tasks: props.tasks
    };
  }

  addTask(newTask) {
    this.setState((prevState) => ({
      tasks: prevState.tasks.concat({
        id: _.sortBy(prevState.tasks, "id").pop().id + 1,
        taskName: newTask.name,
        taskDescription: newTask.desc,
        state: "todo",
      })
    }));
  }

  updateTask(updatedTask) {
    let index = this.state.tasks.findIndex(x=> x.id === updatedTask.id);
    if (index === -1) {
      // Task doesn't exist
    } else {
      let newTasks = {
        tasks: [
          ...this.state.tasks.slice(0, index),
          Object.assign({}, this.state.tasks[index], updatedTask),
          ...this.state.tasks.slice(index + 1),
        ],
      };
      this.setState(newTasks);
    }
  }

  render() {
    return (
      <Container maxWidth="lg">
        <CssBaseline />
        <Grid container spacing={5}>
          <Grid item xs={12} sm={6} lg={3}>
            <CreateTask onAddTask={this.addTask} />
          </Grid>
          <Grid item xs={12} sm={6} lg={3}>
            <TaskList
              title="Todo"
              taskState="todo"
              taskUpdate={this.updateTask}
              items={_.filter(this.state.tasks, { state: "todo" })}
            />
          </Grid>
          <Grid item xs={12} sm={6} lg={3}>
            <TaskList
              title="In Progress"
              taskState="in-progress"
              taskUpdate={this.updateTask}
              items={_.filter(this.state.tasks, { state: "in-progress" })}
            />
          </Grid>
          <Grid item xs={12} sm={6} lg={3}>
            <TaskList
              title="Done"
              taskState="complete"
              taskUpdate={this.updateTask}
              items={_.filter(this.state.tasks, { state: "complete" })}
            />
          </Grid>
        </Grid>
      </Container>
    );
  }
}

export default Board;
