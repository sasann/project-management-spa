import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Board from './components/board';
import registerServiceWorker from './registerServiceWorker';
import seed from './tasks.json';

ReactDOM.render(<Board tasks={seed.tasks}/>, document.getElementById('root'));
registerServiceWorker();
